﻿using UnityEngine;

public interface IMoveState
{
    void Enter();
    void Exit();
    void Loop();
}
