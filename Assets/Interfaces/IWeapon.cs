using UnityEngine;

public interface IWeapon
{
    bool isActive { get; }
    bool CanShoot { get; }
    bool CanReload { get; }
    WeaponSettings Config { get; }

    void Shot();
    void StopShoot();
    void Reload();
    void Activate();
    void Deactivate();
}
