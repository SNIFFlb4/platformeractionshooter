using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "Settings/Character")]
public class CharacterSettings : ScriptableObject
{
    [SerializeField]
    private float _movementSpeed;

    [SerializeField]
    private float _jumpForce;

    [SerializeField]
    private float _dashForce;

    public float MovementSpeed => _movementSpeed;
    public float JumpForce => _jumpForce;
    public float DashForce => _dashForce;
}
