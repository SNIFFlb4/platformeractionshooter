using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Settings/Weapon")]
public class WeaponSettings : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private GameObject _weaponModel;

    [Header("Bullet")]
    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private float _bulletSpeed;
    [SerializeField] private float _bulletDamage;
    [SerializeField] private bool _endlessBullets;

    [Header("Characteristics")]
    [SerializeField] private int _maxAmmoInCase;
    [SerializeField] private int _maxAmmo;
    [SerializeField] private float _timeBetweenShots;
    [SerializeField] private float _timeForReload;

    [Header("Sound")]
    [SerializeField] private AudioClip _shotSound;
    [SerializeField] private AudioClip _reloadSound;

    [Header("UI")]
    [SerializeField] private Sprite _icon;

    public string Name => _name;
    public GameObject WeaponModel => _weaponModel;
    public GameObject BulletPrefab => _bulletPrefab;
    public float BulletSpeed => _bulletSpeed;
    public float BulletDamage => _bulletDamage;
    public bool EndlessBullets => _endlessBullets;
    public int MaxAmmoInCase => _maxAmmoInCase;
    public int MaxAmmo => _maxAmmo;
    public float TimeBetweenShots => _timeBetweenShots;
    public float TimeToReload => _timeForReload;
    public AudioClip ShotSound => _shotSound;
    public AudioClip ReloadSound => _reloadSound;
    public Sprite Icon => _icon;
}
