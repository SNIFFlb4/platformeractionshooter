using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof (AudioSource))]
public abstract class Weapon : MonoBehaviour, IWeapon
{
    public bool CanShoot => (isActive && _currentAmmoInCase>=0 && _isOnReload is false);

    public bool CanReload => (isActive && _currentAmmoInCase!=Config.MaxAmmoInCase && _isOnReload is false);

    public WeaponSettings Config => _config;

    public bool isActive { get; private set; }


    protected abstract void ShotLogic();


    [SerializeField]
    private WeaponSettings _config;

    [SerializeField]
    protected int _currentAmmoInCase;

    [SerializeField]
    protected int _ammoLost;

    protected bool _isOnReload;

    protected WaitForSeconds _shotDelay;
    protected WaitForSeconds _reloadDelay;
    protected AudioSource _audio;

    private void Awake()
    {
        _reloadDelay = new WaitForSeconds(_config.TimeToReload);
        _shotDelay = new WaitForSeconds(_config.TimeBetweenShots);
        _audio = GetComponent<AudioSource>();
    }

    public void Activate()
    {
        isActive = true;
    }

    public void Deactivate()
    {
        StopAllCoroutines();
        isActive = false;
    }

    public virtual void Reload()
    {
        _isOnReload = true;
        StartCoroutine(ReloadCuro());
    }

    private IEnumerator ReloadCuro ()
    {
        _audio.PlayOneShot(Config.ReloadSound);
        yield return _reloadDelay;

        var neededBullets = (_config.MaxAmmoInCase - _currentAmmoInCase);

        if (_ammoLost < neededBullets)
        {
            _currentAmmoInCase += _ammoLost;
            _ammoLost = 0;
        }
        else
        {
            _currentAmmoInCase += neededBullets;
            _ammoLost -= neededBullets;
        }

        _isOnReload = false;
    }

    public void Shot ()
    {
        StartCoroutine(ShootCuro());
    }

    public IEnumerator ShootCuro()
    {
        if (CanShoot is false)
            yield return null;

        _audio.PlayOneShot(Config.ShotSound);
        ShotLogic();

        if (Config.EndlessBullets is false)
        {
            _currentAmmoInCase--;
            if (_currentAmmoInCase <= 0 && CanReload)
            {
                Reload();
                yield break;
            }
        }

        yield return _shotDelay;
        StartCoroutine(ShootCuro());
    }

    public void StopShoot()
    {
        Debug.Log("StopShooting");
        StopAllCoroutines();
    }
}
