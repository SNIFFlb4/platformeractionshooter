using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon
{
    public UnityVFX _mazzleVfx;
    public UnityVFX _bulletVfx;

    protected override void ShotLogic()
    {
        _mazzleVfx?.PlayParticle();
        _bulletVfx?.PlayParticle();
        Debug.Log($"Pistol shot! ( {_currentAmmoInCase} / {_ammoLost} )");
    }
}
