using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugRay : MonoBehaviour
{
    private void Update()
    {
        Debug.DrawLine(transform.position, transform.right * 10, Color.red);
        Debug.DrawLine(transform.position, transform.up * 10, Color.green);
        Debug.DrawLine(transform.position, transform.forward * 10, Color.blue);
    }
}
