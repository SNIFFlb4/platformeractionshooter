using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityVFX : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _vfx;

    public ParticleSystem VFX => _vfx;

    public void PlayParticle()
    {
        _vfx.Play();
    }

    public void StopParticle ()
    {
        _vfx.Stop();
    }
}
