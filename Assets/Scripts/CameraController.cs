using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Camera _camera;

    [SerializeField] private Transform _lookTarget;

    private Vector3 _offset;

    private void Start()
    {
        _offset = _camera.transform.position - _lookTarget.position;
    }

    private void Update()
    {
        _camera.transform.position = _lookTarget.position + _offset;
    }
}
