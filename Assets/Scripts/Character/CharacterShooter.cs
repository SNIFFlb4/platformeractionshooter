using UnityEngine;

public class CharacterShooter : MonoBehaviour
{
    [SerializeField]
    private Transform _weaponHolderR;
    [SerializeField]
    private Transform _weaponHolderL;

    private PlayerControls _controls;
    private CharacterAnimator _charAnimator;

    private IWeapon _currentWeapon;


    public void Init (CharacterAnimator animator, PlayerControls controls, IWeapon weapon)
    {
        this._controls = controls;
        this._charAnimator = animator;
        this._currentWeapon = weapon;
        this._currentWeapon.Activate();

        _controls.MainControls.Shoot.performed += context => Shoot();
        _controls.MainControls.Shoot.canceled += context => StopShoot();
        _controls.MainControls.Reload.performed += context => Reload();
    }

    private void Shoot()
    {
        if (_currentWeapon.CanShoot)
        {
            _currentWeapon.Shot();
        }
    }

    private void StopShoot ()
    {
        _currentWeapon.StopShoot();
    }

    private void Reload()
    {
        if (_currentWeapon.CanReload)
        {
            _currentWeapon.Reload();
            _charAnimator.ReloadAnim();
        }
    }
}
