using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum AnimParameter
{
    IDLE,
    Run,
    RunBack,
    Jump,
    Hanging,
    Shoot,
    Reload
}

public class CharacterAnimator : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;

    //public void ChangeAnimState (CharacterState state)
    //{
    //    switch (state)
    //    {
    //        case CharacterState.IDLE:
    //            IDLE();
    //            break;
    //        case CharacterState.Move:
    //            Run();
    //            break;
    //        case CharacterState.Jump:
    //            Jump(true);
    //            break;
    //        case CharacterState.Hanging:
    //            Hanging(true);
    //            break;
    //        case CharacterState.Reload:
    //            ReloadAnim();
    //            break;
    //        case CharacterState.Shoot:
    //            ShootAnim();
    //            break;
    //    }
    //}

    public void Run()
    {
        _animator.SetBool(AnimParameter.Run.ToString(), true);
    }

    public void RunBack()
    {
        _animator.SetBool(AnimParameter.RunBack.ToString(), true);
    }

    public void IDLE()
    {
        _animator.SetBool(AnimParameter.Run.ToString(), false);
        _animator.SetBool(AnimParameter.RunBack.ToString(), false);
    }

    public void Hanging(bool state)
    {
        _animator.SetBool(AnimParameter.Hanging.ToString(), state);
    }

    public void Jump(bool state)
    {
        _animator.SetBool(AnimParameter.Jump.ToString(), state);
    }

    public void ShootAnim ()
    {
        _animator.SetTrigger(AnimParameter.Shoot.ToString());
    }

    public void ReloadAnim()
    {
        _animator.SetTrigger(AnimParameter.Reload.ToString());
    }
}
