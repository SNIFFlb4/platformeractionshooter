using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewCharacterMovement : MonoBehaviour
{
    private IMoveState _currentState;

    private Dictionary<Type, IMoveState> _moveStates;

    private bool _isInit = false;
    private Rigidbody _rigidbody;
    private PlayerControls _controls;

    private void Awake()
    {
        TryGetComponent<Rigidbody>(out _rigidbody);
    }

    public void Init (CharacterAnimator animator, CharacterSettings settings, PlayerControls controls)
    {
        this._controls = controls;

        _moveStates = new Dictionary<Type, IMoveState>();

        _moveStates[typeof(IdleState)] = new IdleState(animator, _rigidbody);
        _moveStates[typeof(RunState)] = new RunState(_rigidbody, animator, controls, settings);

        _currentState = _moveStates[typeof(IdleState)];
        _currentState.Enter();
        _isInit = true;
    }

    private void Update()
    {
        if (_isInit is false)
            return;

        var input = _controls.MainControls.Move.ReadValue<Vector2>();
        if (input.x == 0)
            ChangeState(_moveStates[typeof(IdleState)]);
        else
            ChangeState(_moveStates[typeof(RunState)]);

        _currentState.Loop();
    }

    private void ChangeState (IMoveState newState)
    {
        if (_currentState.Equals(newState))
            return;

        _currentState.Exit();
        _currentState = newState;
        _currentState.Enter();
    }
}
