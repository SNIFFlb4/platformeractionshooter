using UnityEngine;
using UnityEngine.InputSystem;

enum MoveState { 
    none,
    IDLE,
    Run,
    Jump,
    Dash,
    HangingOnWall,
    HangindOnRope,
    Hook
}


[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour
{
    //����������� ���������� �� �����
    public Quaternion rightDirection;
    public Quaternion leftDirection;
    public Transform groundCheck;

    public Transform wallCheckUp;
    public Transform wallCheckDown;

    [SerializeField]
    private LayerMask groundLayer;

    private float _speed;
    private float _jumpForce;
    private float _dashForce;
    private float _groundCheckRadius;
    private float _wallCkeckUpRadius;
    private float _wallCkeckDownRadius;

    private bool _canMove;
    private bool _onWall;
    private bool _isJumping;
    private bool _onGround;

    private Rigidbody _rigidbody;
    private PlayerControls _controls;
    private CharacterAnimator _characterAnimator;

    private void Awake()
    {
        this._rigidbody = GetComponent<Rigidbody>();
    }

    public void Init (CharacterAnimator animator, CharacterSettings settings, PlayerControls controls)
    {
        this._speed = settings.MovementSpeed;
        this._jumpForce = settings.JumpForce;
        this._dashForce = settings.DashForce;

        this._groundCheckRadius = groundCheck.GetComponent<SphereCollider>().radius;
        this._wallCkeckUpRadius = wallCheckUp.GetComponent<SphereCollider>().radius;
        this._wallCkeckDownRadius = wallCheckDown.GetComponent<SphereCollider>().radius;

        this._controls = controls;
        this._characterAnimator = animator;

        this._controls.MainControls.Jump.performed += Jump;
        this._controls.MainControls.Dash.performed += Dash;
        this._controls.MainControls.Hook.started += Hook_started;

        _canMove = true;
    }

    private void Hook_started(InputAction.CallbackContext obj)
    {
        Debug.Log("The hook is started");
    }

    private void FixedUpdate()
    {
        CheckWall();
        CheckGround();

        Vector2 moveDir = _controls.MainControls.Move.ReadValue<Vector2>();

        if (_canMove is false)
            return;

        if (_onWall)
        {
            MoveOnWall(moveDir);
            return;
        }

        Move(moveDir);
    }

    private void MoveOnWall (Vector2 direction)
    {
        _characterAnimator.Hanging(_onWall);
        _rigidbody.velocity = Vector2.zero;
        _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, direction.y * _speed);
    }

    private void StartHanging(InputAction.CallbackContext obj)
    {



        //if (CanHanging())
        //{
        //    _canMove = false;
        //    _isHanging = true;
        //    _characterAnimator.Hanging(_isHanging);
        //    _rigidbody.isKinematic = true;
        //}
    }

    public void Move(Vector2 direction)
    {
        if (direction.x > 0)
            ChangeModelDirection(rightDirection);
        else if (direction.x < 0)
            ChangeModelDirection(leftDirection);

        _rigidbody.velocity = new Vector3(direction.x * _speed, _rigidbody.velocity.y, _rigidbody.velocity.z);
        _characterAnimator.Run();
    }

    private void ChangeModelDirection (Quaternion lookAt)
    {
        if (transform.localRotation != lookAt)
            transform.localRotation = lookAt;
    }

    private void Jump (InputAction.CallbackContext obj)
    {
        if (_onGround)
        {
            _isJumping = true;
            _characterAnimator.Jump(_isJumping);
            _rigidbody.AddForce(transform.up * _jumpForce, ForceMode.Impulse);
        }

        else if (_onWall)
        {
            _isJumping = true;
            _characterAnimator.Jump(_isJumping);
            Vector2 direction = (transform.up + transform.forward).normalized;
            _rigidbody.AddForce(direction * _jumpForce, ForceMode.Impulse);
        }
    }

    private void CheckGround ()
    {
        var colliders = Physics.OverlapSphere(groundCheck.position, _groundCheckRadius, groundLayer);
        _onGround = (colliders.Length>0);
        _isJumping = !_onGround;
        _characterAnimator.Jump(_isJumping);
    }

    private void CheckWall ()
    {
        var upColliders = Physics.OverlapSphere(wallCheckUp.position, _wallCkeckUpRadius, groundLayer);
        var downColliders = Physics.OverlapSphere(wallCheckDown.position, _wallCkeckDownRadius, groundLayer);

        _onWall = (upColliders.Length > 0 && downColliders.Length > 0);
        Debug.Log($"onWall = {_onWall}");
    }

    private void Dash(InputAction.CallbackContext obj)
    {
        //�������� �� ��������� �������
        _rigidbody.AddForce(transform.forward * _dashForce, ForceMode.Impulse);
    }
}
