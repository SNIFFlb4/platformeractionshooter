﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
class IdleState : IMoveState
{
    private Rigidbody rigidbody;
    private CharacterAnimator animator;


    public IdleState (CharacterAnimator animator, Rigidbody rigidbody)
    {
        this.rigidbody = rigidbody;
        this.animator = animator;
    }

    public void Enter()
    {
        rigidbody.velocity = Vector2.zero;
        animator.IDLE();
    }

    public void Exit()
    {
        Debug.Log("Exit from IDLE state");
    }

    public void Loop()
    {
        Debug.Log("IDLE State - LOOP");
    }
}