﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class RunState : IMoveState
{
    private Rigidbody rigidbody;
    private CharacterAnimator animator;
    private PlayerControls controls;
    private CharacterSettings settings;

    public RunState (Rigidbody rigidbody, CharacterAnimator animator, PlayerControls controls, CharacterSettings settings)
    {
        this.rigidbody = rigidbody;
        this.animator = animator;
        this.controls = controls;
        this.settings = settings;
    }

    public void Enter()
    {
        Debug.Log("Enter to RUN state");
        animator.Run();
    }

    public void Exit()
    {
        Debug.Log("Exit from RUN state");
    }

    public void Loop()
    {
        var input = controls.MainControls.Move.ReadValue<Vector2>();

        rigidbody.velocity = new Vector2(input.x * settings.MovementSpeed, rigidbody.velocity.y);
    }
}
