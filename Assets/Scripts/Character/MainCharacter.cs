using UnityEngine;

public class MainCharacter : MonoBehaviour
{
    public Weapon _weapon;

    //[SerializeField]
    //private CharacterMovement _characterMovement;

    [SerializeField]
    private NewCharacterMovement _characterMovement;

    [SerializeField]
    private CharacterShooter _characterShooter;

    [SerializeField]
    private CharacterAnimator _characterAnimator;

    [SerializeField]
    private CharacterSettings _settings;

    [SerializeField]
    private Transform _target;

    private PlayerControls _controls;

    private void Awake()
    {
        _controls = new PlayerControls();
    }

    private void OnEnable()
    {
        _controls.Enable();
    }

    private void OnDisable()
    {
        _controls.Disable();
    }

    private void Start()
    {
        _characterMovement.Init(_characterAnimator, _settings, _controls);
        _characterShooter.Init(_characterAnimator, _controls, _weapon);
    }
}
