using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterState
{
    IDLE,
    Move,
    Jump,
    Hanging,
    Shoot,
    Reload,
    Dash,
    Hook
}

public class CharacterStateMachine : MonoBehaviour
{
    private CharacterState currentState;
    private CharacterAnimator _animator;

    public void Init (CharacterAnimator animator)
    {
        this._animator = animator;
    }

    public void ChangeState (CharacterState newState)
    {
        if (currentState.Equals(newState))
            return;

        Debug.Log($"Change state from {currentState} to {newState}");
        currentState = newState;
        //_animator.ChangeAnimState(currentState);
    }
}
