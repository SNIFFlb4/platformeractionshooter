using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovement : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _rigidbody;

    [SerializeField]
    private float _speed;

    private float xDirection;

    private void Update()
    {
        xDirection = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate()
    {
        Move(xDirection);
    }

    private void Move (float xDirection)
    {
        Vector3 direction = new Vector3(xDirection * _speed, _rigidbody.velocity.y, _rigidbody.velocity.z);
        _rigidbody.velocity = direction;
    }
}
